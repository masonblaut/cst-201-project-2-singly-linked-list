// Mason Blaut
// This is my own work.
#include "SinglyLinkedList.h"
using namespace std;

SinglyLinkedList::SinglyLinkedList()
{
	head = NULL;
	tail = NULL;
}

SinglyLinkedList::SinglyLinkedList(SinglyLinkedList& obj)
{
	Node* newPtr;
	Node* oldPtr;

	this->head = NULL;
	this->tail = NULL;

	oldPtr = obj.head;

	if (oldPtr != NULL)
	{
		Node *newNode = new Node();
		newNode->val = oldPtr->val;

		this->head = newNode;
		newPtr = newNode;
		oldPtr = oldPtr->next;

		while (oldPtr->next != NULL) 
		{
			Node* newNode = new Node;
			newNode->val = oldPtr->val;
			newPtr->next = newNode;
			newPtr = newPtr->next;

			oldPtr = oldPtr->next;
		}
		Node* lastNode = new Node;
		lastNode->val = oldPtr->val;
		lastNode->next = NULL;
		newPtr->next = lastNode;
		lastNode = NULL;
	}
}




Node* SinglyLinkedList::front()
{
	return head;
}

Node* SinglyLinkedList::last()
{
	Node* temp = head;
	if (temp == NULL) 
	{
		return nullptr;
	}
	else 
	{
		while (temp->next != NULL)
		{
			temp = temp->next;
		}
		tail = temp;
		return tail;
	}
}

Node* SinglyLinkedList::getHead()
{
	return head;
}

void SinglyLinkedList::insertValStart(int val)
{
	Node* node = new Node();
	node->val = val;
	node->next = NULL;
	insertNodeStart(node);
}

void SinglyLinkedList::insertNodeStart(Node* node)
{
	node->next = head;
	head = node;
}

void SinglyLinkedList::deleteNodeStart()
{
	Node* temp = new Node();
	temp = head;
	head = head->next;
	delete temp;
}

void SinglyLinkedList::deleteNodeEnd()
{
	Node* temp = head;
	Node* temp2 = new Node();
	if (temp == NULL)
	{
		return;
	}
	else
	{
		if (temp->next != NULL)
		{
			while (temp->next->next != NULL)
			{
				temp = temp->next;
			}
			temp->next = temp2;
			temp->next = NULL;
			delete temp2;
		}
	}
}

bool SinglyLinkedList::isEmpty()
{
	if (head != NULL)
	{
		return false;
	}
	else
	{
		return true;
	}
}

int SinglyLinkedList::size()
{
	Node* temp = head;
	int size = 0;
	if (temp == NULL)
	{
		return 0;
	}
	else
	{
		while (temp->next != NULL)
		{
			temp = temp->next;
			size++;
		}
		size++;
		return size;
	}
}

void SinglyLinkedList::reverseList()
{
	Node* prev = new Node();
	Node* curr = head;
	Node* nextNode = new Node();

	if (curr == NULL)
	{
		return;
	}
	else
	{
		while (curr != NULL)
		{
			nextNode = curr->next;
			curr->next = prev;
			prev = curr;
			curr = nextNode;
		}
		head = prev;
	}
	deleteNodeEnd();
}

void SinglyLinkedList::mergeList(SinglyLinkedList& obj)
{
	Node* theLast = last();
	theLast->next = obj.getHead();
}

void SinglyLinkedList::print()
{

	Node* temp = head;
	if (temp == NULL)
	{
		cout << "The list is empty.\n";
	}
	else
	{
		while (temp->next != NULL)
		{
			cout << temp->val << " ";
			temp = temp->next;
		}
		if (temp->next == NULL)
		{
			cout << temp->val << "\n";
		}
	}
}
