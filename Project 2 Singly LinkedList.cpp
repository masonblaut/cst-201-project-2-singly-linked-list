// Mason Blaut
// This is my own work.

#include <iostream>
#include "Node.h"
#include "SinglyLinkedList.h"
using namespace std;

int main()
{
	//constructing the linked list
	SinglyLinkedList theList;

	//test the insert value method
	cout << "After inserting the values into the list: \n";
	theList.insertValStart(1);
	theList.insertValStart(2);
	theList.insertValStart(3);
	theList.insertValStart(4);
	theList.print();

	cout << "\n";

	//testing the remove functions. Will delete only 1 and 4.
	cout << "After removing the first and last values into the list: \n";
	theList.deleteNodeEnd();
	theList.deleteNodeStart();
	theList.print();

	cout << "\n";

	//testing the front and last methods
	cout << "The first value in this list is ";
	cout << theList.front()->val << "\n";
	cout << "\n";
	cout << "The last value in this list is ";
	cout << theList.last()->val << "\n";
	cout << "\n";

	//tests the reverse method
	cout << "After reversing the list: \n";
	theList.reverseList();
	theList.print();

	cout << "\n";

	//testing the copy constructor
	cout << "After copying the list: \n";
	cout << "List 2: ";
	SinglyLinkedList *secondList = new SinglyLinkedList(theList);
	secondList->print();
	cout << "\n";

	cout << "\n";

	//testing the merge method
	cout << "After merging list 2 into list 1: \n";
	theList.mergeList(*secondList);
	theList.print();

	cout << "\n";
    cout << "END\n";
}