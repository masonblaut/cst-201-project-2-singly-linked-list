// Mason Blaut
// This is my own work.

#pragma once
#pragma once
#include "Node.h"
#include <cstddef>
#include <iostream>
#include <stdlib.h>

class SinglyLinkedList
{
protected:
	Node* head;
	Node* tail;
public:
	SinglyLinkedList(); //default constructor
	SinglyLinkedList(struct SinglyLinkedList &obj);
	Node* front(); // access to the first element
	Node* last(); // access to the last element
	Node* getHead(); //returns the head of the current list
	void insertValStart(int val); //calls the insertNodeStart method and places a new value at the front of the list
	void insertNodeStart(Node* node);
	void deleteNodeStart(); //deletes the node and value at the start
	void deleteNodeEnd(); // deletes the node and value at the end of the list
	bool isEmpty(); // checks to see if the list is empty
	int size(); //returns the number of elements in the list
	void reverseList(); //reverses the list order
	void mergeList(struct SinglyLinkedList &obj);
	void print(); // prints contents of list
};

